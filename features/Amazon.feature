Feature: Task Amazon Testing
	Scenario: Negative Sign In (Email Not Registered)
		Given Open Amazon "https://www.amazon.com/"
		Then Click Sign In
		Then Input invalid email "dianitananda@gmail.com" 
		Then Click continue
		Then Showing error message "We cannot find an account with that email address"

	Scenario: Negative Sign In (Input Blank Email)
		Given Open Amazon "https://www.amazon.com/"
		Then Click Sign In
		Then Leave email to be blank ""
		Then Click continue
		Then Showing error message below your email "Enter your email or mobile phone number"

	Scenario: Negative Sign Up (Input Invalid Email Format)
		Given Open Amazon "https://www.amazon.com/"
		Then Click Sign In
		Then You must create Amazon account first
		Then Enter your name "Dianita Ananda"
		Then Tried to enter invalid Email format "lalalala"
		Then Enter your password "R4hadian"
		Then Please re-enter your password "R4hadian"
		Then Continue to Create your account
		Then Showing error label below your email "Enter a valid email address"

	Scenario: Negative Sign Up (Password didn't match with re-enter Password)
		Given Open Amazon "https://www.amazon.com/"
		Then Click Sign In
		Then You must create Amazon account first
		Then Enter your name "Dianita Ananda"
		Then Tried to enter valid Email format "dianitananda@gmail.com"
		Then Enter your password "R4hadian"
		Then Tried to re-enter different from your password "Rah4dian"
		Then Continue to Create your account
		Then Showing error label below your re-enter password "Passwords must match"