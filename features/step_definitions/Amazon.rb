require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome

Given (/^Open Amazon "(.*?)"$/) do |amazon|
    driver.navigate.to amazon
end

Then ("Click Sign In") do
    driver.find_element(:xpath,'//*[@id="nav-link-accountList"]').click
end

Then (/^Input invalid email "(.*?)"$/) do |invalidEmail|
    driver.find_element(:xpath, '//*[@id="ap_email"]').send_keys(invalidEmail)
end

Then ("Click continue") do
    driver.find_element(:xpath, '//*[@id="continue"]').click
end

Then (/^Showing error message "(.*?)"$/) do |errMsg1|
    errMsg = driver.find_element(:xpath, '//*[@id="auth-error-message-box"]/div/div/ul/li/span').text()
    errMsg1.match(errMsg)
end

Then (/^Showing error message below your email "(.*?)"$/) do |errMsg1|
    errMsg = driver.find_element(:xpath, '//*[@id="auth-email-missing-alert"]/div/div').text()
    errMsg1.match(errMsg)
end

Then (/^Leave email to be blank "(.*?)"$/) do |blank|
    driver.find_element(:xpath, '//*[@id="ap_email"]').send_keys(blank)
end

Then ("You must create Amazon account first") do
    driver.find_element(:xpath, '//*[@id="createAccountSubmit"]').click
end

Then (/^Enter your name "(.*?)"$/) do |yourName|
    driver.find_element(:xpath, '//*[@id="ap_customer_name"]').send_keys(yourName)
end

Then (/^Tried to enter invalid Email format "(.*?)"$/) do |invalidEmail|
    driver.find_element(:xpath, '//*[@id="ap_email"]').send_keys(invalidEmail)
end

Then (/^Enter your password "(.*?)"$/) do |password|
    driver.find_element(:xpath, '//*[@id="ap_password"]').send_keys(password)
end

Then (/^Please re-enter your password "(.*?)"$/) do |rePassword|
    driver.find_element(:xpath, '//*[@id="ap_password_check"]').send_keys(rePassword)
end

Then ("Continue to Create your account") do
    driver.find_element(:xpath, '//*[@id="continue"]').click
end

Then (/^Showing error label below your email "(.*?)"$/) do |errMsg1|
    errEmailSU = driver.find_element(:xpath, '//*[@id="auth-email-invalid-email-alert"]/div/div').text()
    errMsg1.match(errEmailSU)
end

Then (/^Tried to enter valid Email format "(.*?)"$/) do |email|
    driver.find_element(:xpath, '//*[@id="ap_email"]').send_keys(email)
end

Then (/^Tried to re-enter different from your password "(.*?)"$/) do |invalidrePassword|
    driver.find_element(:xpath, '//*[@id="ap_password_check"]').send_keys(invalidrePassword)
end

Then (/^Showing error label below your re-enter password "(.*?)"$/) do |errMsg1|
    errRePW = driver.find_element(:xpath, '//*[@id="auth-password-mismatch-alert"]/div/div').text()
    errMsg1.match(errRePW)
    driver.close
end