require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome
wait = Selenium::WebDriver::Wait.new(:timeout => 20)

Given(/^Open Jurnal "(.*?)"$/) do |jurnal|
    driver.navigate.to jurnal
end

Then(/^Enter your Jurnal email "(.*?)"$/) do |email|
    driver.find_element(:xpath, '//*[@id="user_email"]').send_keys(email)
end

Then(/^Enter your Jurnal password "(.*?)"$/) do |password|
    driver.find_element(:xpath, '//*[@id="user_password"]').send_keys(password)
end

Then("Click Sign In Button") do
    driver.find_element(:xpath, '//*[@id="new-signin-button"]').click
end

Given("Try Create Invoice") do
driver.find_element(:xpath, '//*[@id="manage-invoice-table"]/tbody/tr[3]/td[1]/a').click
end

Then("Choose dropdown and add new Customer") do
   driver.find_element(:xpath, '//*[@id="select2-chosen-19"]').click
   addCust = wait.until {
       driver.find_element(:xpath, '//*[@id="select2-result-label-0"]')
}
addCust.click
end

Then(/^Input Customer Name "(.*?)"$/) do |custName|
    wait.until{
        driver.find_element(:xpath, '//*[@id="add-customer"]/div[2]/div[2]').displayed?
    }
    driver.find_element(:xpath, '//*[@id="person_display_name"]').send_keys(custName)
end

Then(/^Input Customer Email "(.*?)"$/) do |custEmail|
    driver.find_element(:xpath, '//*[@id="person_email"]').send_keys(custEmail)
end

Then(/^Input Customer Billing Address "(.*?)"$/) do |custBilling|
    driver.find_element(:xpath, '//*[@id="person_billing_address"]').send_keys(custBilling)
end

Then(/^Input Customer Phone Number "(.*?)"$/) do |custPhone|
    driver.find_element(:xpath, '//*[@id="person_phone"]').send_keys(custPhone)
end

Then("Save Customer") do
    driver.find_element(:xpath, '//*[@id="aodc-add"]').click
end


Given(/^Input Transaction Date "(.*?)"$/) do |trxDate|
    driver.find_element(:xpath, '//*[@id="transaction_transaction_date"]').clear
    driver.find_element(:xpath, '//*[@id="transaction_transaction_date"]').send_keys(trxDate)
end


Then(/^Input Due Date "(.*?)"$/) do |dueDate|
    sleep(3)
    driver.find_element(:xpath, '//*[@id="transaction_due_date"]').click
    sleep(3)
    driver.find_element(:xpath, '//*[@id="transaction_due_date"]').clear
    driver.find_element(:xpath, '//*[@id="transaction_due_date"]').send_keys(dueDate)
end

Given("Select Item from the List") do
    driver.find_element(:xpath, '//*[@id="select2-chosen-20"]').click
    sleep(3)
    driver.find_element(:xpath, '//*[@id="select2-results-20"]/li[2]').click
    sleep(3)
end
And(/^Add Unit Price "(.*?)"$/) do |unitPrice|
    driver.find_element(:xpath, '//*[@id="transaction_transaction_lines_attributes_0_rate"]').click
    driver.find_element(:xpath, '//*[@id="transaction_transaction_lines_attributes_0_rate"]').send_keys(unitPrice)
    driver.find_element(:xpath, '//*[@id="transaction_transaction_lines_attributes_0_amount"]').click
    sleep(3)
end

And("Create Invoice") do
    driver.find_element(:xpath, '//*[@id="create_button"]').click
    sleep(3)
end




