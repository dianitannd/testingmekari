Feature: Task Jurnal Testing Creating Invoice
	Scenario: Sign In
		Given Open Jurnal "http://sandbox.jurnal.id/users/login"
		Then Enter your Jurnal email "dianitananda@gmail.com"
        Then Enter your Jurnal password "12345678"
        Then Click Sign In Button

    Scenario: Creating Customer
        Given Try Create Invoice
        Then Choose dropdown and add new Customer
        Then Input Customer Name "Dianita Ananda Eka Putri"
        Then Input Customer Email "ananda.eka@test.com"
        Then Input Customer Billing Address "Jl. Purwakarta No. 189B, Duren Jaya - Bekasi Timur"
        Then Input Customer Phone Number "089977665544"
        Then Save Customer

    Scenario: Input Date
        Given Input Transaction Date "07/07/2020"
        And Input Due Date "07/09/2020"

    Scenario: Select Product and Create Invoice
        Given Select Item from the List
        And Add Unit Price "200000"
        And Create Invoice